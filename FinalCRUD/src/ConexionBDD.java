import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionBDD {

    // libreria de base de datos
    String driver = "com.mysql.cj.jdbc.Driver";

    // Nombre de la base de datos
    String bd = "unidad4";

    // host
    String hostname = "localhost";

    // puerto
    String puerto = "3306";

    // Ruta de datos
    String url = "jdbc:mysql://" + hostname + ":" + puerto + "/" + bd
            + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatatimeCode=false&serverTimezone=UTC";
    // String url = "jdbc:mysql://
    // localhost:3306/prueba2?useUnicode=true&useJDBCCompliant
    // TimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";;

    // Nombre de usuario
    String usuario = "root";

    // Contraseña
    String pass = "katakuri2002";

    public Connection Conexion() {

        Connection respuesta = null;

        try {

            Class.forName(driver);
            // System.out.println("Driver se cargo exitosamente");
            respuesta = DriverManager.getConnection(url, usuario, pass);
            // System.out.println("Conexion exitosa");

        } catch (ClassNotFoundException | SQLException e) {

            System.out.println(e);
            // System.out.println("No se pudo realizar la conexion");

        }

        return respuesta;
    }

}