import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public class CRUD {

    ConexionBDD bd = new ConexionBDD();
    PreparedStatement sentencia = null;
    ResultSet resultado = null;

    public void consultar() {
        DefaultTableModel modelo;
        Connection respuesta = null;
        respuesta = bd.Conexion();

        try {
            // prepara sentencia
            sentencia = respuesta.prepareStatement("SELECT * FROM tablita");

            // ejecuto la sentencia
            resultado = sentencia.executeQuery();

            Object datos[] = new Object[6];
            modelo = (DefaultTableModel) Panel.miTabla.getModel();
            int filas = Panel.miTabla.getRowCount();
            while (resultado.next()) {
                datos[0] = resultado.getString("NdeControl");
                datos[1] = resultado.getString("Nombre");
                datos[2] = resultado.getString("Apellidos");
                datos[3] = resultado.getString("Email");
                datos[4] = resultado.getString("telefono");
                datos[5] = resultado.getString("Sexo");
                modelo.addRow(datos);
            }
            respuesta.close();
            for (int i = filas - 1; i >= 0; i--) {
                Panel.modeloTabla.removeRow(i);

            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error al consultar la tabla");
        }

    }

    public boolean busqueda() {
        DefaultTableModel modelo;
        Connection respuesta = null;
        respuesta = bd.Conexion();
        String ndeControl = Panel.campo5.getText();
        boolean encontrado = false;
        try {
            // prepara sentencia
            sentencia = respuesta.prepareStatement("SELECT * FROM tablita");

            // ejecuto la sentencia
            resultado = sentencia.executeQuery();

            Object datos[] = new Object[6];
            modelo = (DefaultTableModel) Panel.miTabla.getModel();
            int filas = Panel.miTabla.getRowCount();
            while (resultado.next()) {
                datos[0] = resultado.getString("NdeControl");
                datos[1] = resultado.getString("Nombre");
                datos[2] = resultado.getString("Apellidos");
                datos[3] = resultado.getString("Email");
                datos[4] = resultado.getString("telefono");
                datos[5] = resultado.getString("Sexo");
                if (datos[0].equals(ndeControl) || datos[1].equals(ndeControl) || datos[2].equals(ndeControl)
                        || datos[3].equals(ndeControl) || datos[4].equals(ndeControl) || datos[5].equals(ndeControl)) {
                    encontrado = true;
                }
                modelo.addRow(datos);
            }
            respuesta.close();
            for (int i = filas - 1; i >= 0; i--) {
                Panel.modeloTabla.removeRow(i);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return encontrado;

    }

    public void modificar(String ndeControl, String nombre, String apellidos, String email, String telefono,
            String sexo) {
        Connection respuesta = null;

        if (!nombre.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET nombre=? WHERE NdeControl=?");
                sentencia.setString(1, nombre);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        if (!nombre.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET nombre=? WHERE NdeControl=?");
                sentencia.setString(1, nombre);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        if (!apellidos.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET apellidos=? WHERE NdeControl=?");
                sentencia.setString(1, apellidos);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (!email.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET email=? WHERE NdeControl=?");
                sentencia.setString(1, email);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (!telefono.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET telefono=? WHERE NdeControl=?");
                sentencia.setString(1, telefono);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (!sexo.equals("Seleccione")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET sexo=? WHERE NdeControl=?");
                sentencia.setString(1, sexo);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    public void cambio(String ndeControlNuevo, String ndeControl) {
        Connection respuesta = null;

        if (!ndeControl.equals("")) {
            try {
                respuesta = bd.Conexion();
                sentencia = respuesta.prepareStatement("UPDATE tablita SET ndeControl=? WHERE ndeControl=?");
                sentencia.setString(1, ndeControlNuevo);
                sentencia.setString(2, ndeControl);
                sentencia.executeUpdate();
                respuesta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    public void insertar(String ndeControl, String nombre, String apellidos, String email, String telefono,
            String sexo) {
        Connection respuesta = null;
        respuesta = bd.Conexion();

        try {
            sentencia = respuesta.prepareStatement("INSERT INTO tablita VALUES(?,?,?,?,?,?)");
            sentencia.setString(1, ndeControl);
            sentencia.setString(2, nombre);
            sentencia.setString(3, apellidos);
            sentencia.setString(4, email);
            sentencia.setString(5, telefono);
            sentencia.setString(6, sexo);

            sentencia.executeUpdate();
            // System.out.println("Se insertaron los datos correctamente");
            respuesta.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean eliminar(String idEliminar, boolean correcto) {
        Connection respuesta = null;
        respuesta = bd.Conexion();

        try {
            sentencia = respuesta.prepareStatement("DELETE FROM tablita WHERE NdeControl=?");
            sentencia.setString(1, idEliminar);
            sentencia.executeUpdate();
            respuesta.close();
            correcto = true;
            return correcto;
        } catch (SQLException e) {
            e.printStackTrace();
            return correcto;
        }
    }

}
