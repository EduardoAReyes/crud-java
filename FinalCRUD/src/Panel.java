import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Panel extends JPanel {

    private JLabel nombres, apellidos, email, celular, direccion, sexo, foto;
    public static JTextField campo1, campo2, campo3, campo4, campo5;
    private JButton guardar, eliminar2, modificar, consultar, limpiar, cambiar;
    public static JComboBox seleccione;
    public static Object datos[][] = {};
    public static JTable miTabla;
    public static DefaultTableModel modeloTabla;
    GridBagConstraints contenedor = new GridBagConstraints();

    public Panel() {
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.WHITE);
        crearCampos();
        crearEtiquetas();
        crearBotones();
        crearTabla();
        crearCombo();
        AccionBoton a = new AccionBoton(guardar, eliminar2, modificar, consultar, limpiar, seleccione, cambiar);
    }

    private void crearCombo() {
        seleccione = new JComboBox();
        seleccione.addItem("Seleccione");
        seleccione.addItem("Masculino");
        seleccione.addItem("Femenino");
        contenedor.gridx = 4;
        contenedor.gridy = 8;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        contenedor.insets = new Insets(0, 0, 50, 0);
        this.add(seleccione, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);
    }

    private void crearTabla() {

        modeloTabla = new DefaultTableModel();
        miTabla = new JTable(modeloTabla);
        JScrollPane scrollTabla;

        String nombreColumnas[] = { "NdeControl", "Nombres", "Apellidos", "Edad", "Télefono", "Sexo" };

        modeloTabla = new DefaultTableModel(datos, nombreColumnas);
        miTabla = new JTable(modeloTabla);
        scrollTabla = new JScrollPane(miTabla);
        scrollTabla.setPreferredSize(new java.awt.Dimension(50, 150));

        miTabla = new JTable(modeloTabla);
        contenedor.gridx = 0;
        contenedor.gridy = 11;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 6;
        contenedor.insets = new Insets(10, 0, 0, 0);
        this.add(scrollTabla, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

    }

    private void crearBotones() {

        guardar = new JButton("Guardar");
        contenedor.gridx = 0;
        contenedor.gridy = 12;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        contenedor.insets = new Insets(20, 0, 0, 0);
        this.add(guardar, contenedor);

        eliminar2 = new JButton("Eliminar");
        contenedor.gridx = 2;
        contenedor.gridy = 12;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(20, 0, 0, 0);
        this.add(eliminar2, contenedor);

        modificar = new JButton("Modificar");
        contenedor.gridx = 3;
        contenedor.gridy = 12;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(20, 0, 0, 0);
        this.add(modificar, contenedor);

        consultar = new JButton("Consultar");
        contenedor.gridx = 4;
        contenedor.gridy = 12;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(20, 0, 0, 0);
        this.add(consultar, contenedor);

        limpiar = new JButton("Limpiar");
        contenedor.gridx = 5;
        contenedor.gridy = 12;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(20, 0, 0, 0);
        this.add(limpiar, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        cambiar = new JButton("Cambiar");
        contenedor.gridx = 0;
        contenedor.gridy = 8;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        this.add(cambiar, contenedor);

    }

    private void crearCampos() {
        contenedor.fill = GridBagConstraints.HORIZONTAL;
        campo1 = new JTextField();
        contenedor.gridx = 0;
        contenedor.gridy = 1;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 3;
        this.add(campo1, contenedor);

        campo2 = new JTextField();
        contenedor.gridx = 0;
        contenedor.gridy = 3;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 3;
        this.add(campo2, contenedor);

        campo3 = new JTextField();
        contenedor.gridx = 0;
        contenedor.gridy = 5;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        this.add(campo3, contenedor);

        campo4 = new JTextField();
        contenedor.gridx = 2;
        contenedor.gridy = 5;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(0, 10, 0, 0);
        this.add(campo4, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        campo5 = new JTextField();
        contenedor.gridx = 0;
        contenedor.gridy = 8;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 3;
        contenedor.insets = new Insets(0, 0, 50, 0);
        this.add(campo5, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

    }

    private void crearEtiquetas() {
        nombres = new JLabel("Nombres:");
        contenedor.gridx = 0;
        contenedor.gridy = 0;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        contenedor.insets = new Insets(0, 0, 0, 30);
        this.add(nombres, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        apellidos = new JLabel("Apellidos:");
        contenedor.gridx = 0;
        contenedor.gridy = 2;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 2;
        contenedor.insets = new Insets(20, 0, 0, 30);
        this.add(apellidos, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        email = new JLabel("Edad:");
        contenedor.gridx = 0;
        contenedor.gridy = 4;
        contenedor.gridheight = 2;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(30, 0, 65, 30);
        this.add(email, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        celular = new JLabel("Celular:");
        contenedor.gridx = 2;
        contenedor.gridy = 4;
        contenedor.gridheight = 2;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(30, 10, 65, 0);
        this.add(celular, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        direccion = new JLabel("N.deControl:");
        contenedor.gridx = 0;
        contenedor.gridy = 7;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(30, 0, 0, 0);
        this.add(direccion, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        sexo = new JLabel("Sexo");
        contenedor.gridx = 4;
        contenedor.gridy = 7;
        contenedor.gridheight = 1;
        contenedor.gridwidth = 1;
        contenedor.insets = new Insets(30, 0, 0, 80);
        this.add(sexo, contenedor);
        contenedor.insets = new Insets(0, 0, 0, 0);

        foto = new JLabel();
        contenedor.gridx = 4;
        contenedor.gridy = 0;
        contenedor.gridheight = 6;
        contenedor.gridwidth = 2;

        ImageIcon imagenMov = new ImageIcon(getClass().getResource("LogoMysql.png"));
        ImageIcon imageIcon1Mov = new ImageIcon(imagenMov.getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT));
        foto.setIcon(imageIcon1Mov);
        this.add(foto, contenedor);

    }

    public void Nombre() {

        System.out.println("Reyes Rosales Eduardo Alberto");

        System.out.println("07-04-2022");

    }

}