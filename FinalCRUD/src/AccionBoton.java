import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class AccionBoton implements ActionListener, ItemListener {

    /*
     * 
     * CRUD crud = new CRUD();
     * /*
     * ConexionBDD bd = new ConexionBDD();
     * CRUD b = new CRUD();
     * Connection respuesta = null;
     * respuesta = bd.Conexion();
     */

    // crud.insertar(0, 0, 0, "cero");
    // crud.eliminar(1, false);
    // crud.modificar(2, 0, 0, "");
    // crud.leer();

    CRUD c = new CRUD();
    private JButton guardar, eliminar2, modificar, consultar, limpiar, cambiar;
    private JComboBox seleccione;

    public AccionBoton(JButton guardar, JButton eliminar2, JButton modificar, JButton consultar, JButton limpiar,
            JComboBox seleccione, JButton cambiar) {
        this.guardar = guardar;
        this.eliminar2 = eliminar2;
        this.modificar = modificar;
        this.consultar = consultar;
        this.limpiar = limpiar;
        this.seleccione = seleccione;
        this.cambiar = cambiar;

        guardar.addActionListener(this);
        eliminar2.addActionListener(this);
        modificar.addActionListener(this);
        consultar.addActionListener(this);
        limpiar.addActionListener(this);
        cambiar.addActionListener(this);
        seleccione.addItemListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        if (e.getSource() == guardar) {
            String ndeControl = Panel.campo5.getText();
            String nombre = Panel.campo1.getText();
            String apellidos = Panel.campo2.getText();
            String email = Panel.campo3.getText();
            String telefono = Panel.campo4.getText();
            String sexo = Panel.seleccione.getSelectedItem().toString();

            if (ndeControl.equals("")) {
                JOptionPane.showMessageDialog(null, "No se encontro numero de control");
            } else {
                c.insertar(ndeControl, nombre, apellidos, email, telefono, sexo);
                JOptionPane.showMessageDialog(null, "Los datos se guardaron exitosamente");

                Panel.campo1.setText("");
                Panel.campo2.setText("");
                Panel.campo3.setText("");
                Panel.campo4.setText("");
                Panel.campo5.setText("");
                Panel.seleccione.removeAllItems();
                Panel.seleccione.addItem("Seleccione");
                Panel.seleccione.addItem("Masculino");
                Panel.seleccione.addItem("Femenino");
                c.consultar();
            }
        }

        if (e.getSource() == eliminar2) {

            if (c.busqueda() == true) {
                String ndeControl = Panel.campo5.getText();
                c.eliminar(ndeControl, false);
                JOptionPane.showMessageDialog(null, "Se elimino correctamente");
                Panel.campo5.setText("");
                c.consultar();

            } else {
                JOptionPane.showMessageDialog(null, "No se encontro numero de control");
            }

        }

        if (e.getSource() == modificar) {

            String ndeControl = Panel.campo5.getText();
            String nombre = Panel.campo1.getText();
            String apellidos = Panel.campo2.getText();
            String email = Panel.campo3.getText();
            String telefono = Panel.campo4.getText();
            String sexo = Panel.seleccione.getSelectedItem().toString();

            if (c.busqueda() == true) {
                c.modificar(ndeControl, nombre, apellidos, email, telefono, sexo);
                JOptionPane.showMessageDialog(null, "Se actualizo correctamente");
                Panel.campo1.setText("");
                Panel.campo2.setText("");
                Panel.campo3.setText("");
                Panel.campo4.setText("");
                Panel.seleccione.removeAllItems();
                Panel.seleccione.addItem("Seleccione");
                Panel.seleccione.addItem("Masculino");
                Panel.seleccione.addItem("Femenino");
                c.consultar();

            } else {
                JOptionPane.showMessageDialog(null, "No se encontro numero de control");
            }

        }

        if (e.getSource() == consultar) {
            c.consultar();

        }

        if (e.getSource() == limpiar) {
            Panel.campo1.setText("");
            Panel.campo2.setText("");
            Panel.campo3.setText("");
            Panel.campo4.setText("");
            Panel.campo5.setText("");
            Panel.seleccione.removeAllItems();
            Panel.seleccione.addItem("Seleccione");
            Panel.seleccione.addItem("Masculino");
            Panel.seleccione.addItem("Femenino");
        }

        if (e.getSource() == cambiar) {
            String ndeControl = Panel.campo5.getText();

            if (c.busqueda() == true) {

                String ndeControlNuevo = JOptionPane.showInputDialog(null, "Introdusca el nuevo numero de control");
                c.cambio(ndeControlNuevo, ndeControl);
                JOptionPane.showMessageDialog(null, "Se actualizo el numero de control correctamente");
                Panel.campo1.setText("");
                Panel.campo2.setText("");
                Panel.campo3.setText("");
                Panel.campo4.setText("");
                Panel.campo5.setText("");
                Panel.seleccione.removeAllItems();
                Panel.seleccione.addItem("Seleccione");
                Panel.seleccione.addItem("Masculino");
                Panel.seleccione.addItem("Femenino");
                c.consultar();

            } else {
                JOptionPane.showMessageDialog(null, "No se encontro numero de control");
            }

        }

    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        // TODO Auto-generated method stub

    }

}
