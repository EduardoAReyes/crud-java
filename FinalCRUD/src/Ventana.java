import javax.swing.JFrame;

public class Ventana extends JFrame {

    public Ventana() {
        // Inicializar variables
        this.setTitle("Reyes Rosales Eduardo Alberto");
        this.setSize(570, 700);
        // this.setResizable(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        colocarPanel();
    }

    private void colocarPanel() {
        Panel instancia = new Panel();
        this.setContentPane(instancia);
    }

}